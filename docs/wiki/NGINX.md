NGINX Server Block
------------------

	server {
		listen 80 default_server;
		listen [::]:80 default_server;

		server_name _;
		root /var/www/html;
		index index.php;

		location / {
			try_files $uri $uri/ =404;
		}

		location ~ /(includes|lib|res|docs) {
			deny all;
			return 404;
		}

		location /admin {
			auth_basic 'Restricted Content';
			auth_basic_user_file /var/www/html/.htpasswd;
		}

		location ~ \.php$ {
			include snippets/fastcgi-php.conf;
			fastcgi_pass unix:/var/run/php5-fpm.sock;
		}
	}
