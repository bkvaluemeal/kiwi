Kiwi CMS Wiki
-------------

Kiwi CMS, by default, does not use a database unlike most other content
management systems (CMS). Why incur the extra overhead of accessing a database
when most (if not all) of your content is static or will rarely ever be updated?

How does it work?
-----------------

Kiwi CMS directly modifies the pages on your webserver, and imports them when
the page is loaded. If you thought a system like WordPress was fast, this is
faster. The only bottleneck is the speed PHP can render the data.

Who is it for?
--------------

Because Kiwi CMS relies on importing static content, this system is perfect for
blogs, news sites, projects, etc... but it's functionality can be extended with
custom PHP written right into the page. When the page is imported, the code will
get executed as usual allowing any dynamic content you wish.

How do I start?
---------------

Simply download and extract this project to your web server like so...

	curl https://dev.bkvaluemeal.net/kiwi.git/tarball/master | tar xv

... and configure your server block to secure it. See the example below:

- [NGINX]

The default username and password to the admin area is admin:admin, but you
should change this by generating a new .htpasswd file with your own information.

[NGINX]: https://dev.bkvaluemeal.net/kiwi.git/blob/master/docs/wiki/NGINX.md
