<?php
	function getPage($page)
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/includes/header.php');
		require_once($_SERVER['DOCUMENT_ROOT'].'/includes/body/'.$page);
		require_once($_SERVER['DOCUMENT_ROOT'].'/includes/footer.php');
	}

	function getPages($dir = '')
	{
		$result = array();

		foreach (scandir($_SERVER['DOCUMENT_ROOT'].'/includes/body/'.$dir) as $obj)
		{
			if ($obj !== '.' && $obj !== '..')
			{
				if (strstr($obj, '.php'))
				{
					array_push($result, $dir.$obj);
				}
				else
				{
					$result = array_merge($result, getPages($dir.$obj.'/'));
				}
			}
		}

		return $result;
	}

	function writePage($page, $body)
	{
		$tab = TRUE;
		if (strstr($page, 'css'))
		{
			$tab = FALSE;
			$page = '/'.$page;
		}
		else
		{
			$page = '/includes/'.$page;
		}

		$handle = fopen($_SERVER['DOCUMENT_ROOT'].$page, 'w') or die('Unable to edit '.$page);

		if ($handle)
		{
			foreach (explode("\r\n", $body) as $line)
			{
				if (!empty($line))
				{
					if ($tab)
					{
						fwrite($handle, utf8_encode("\t".$line.PHP_EOL));
					}
					else
					{
						fwrite($handle, utf8_encode($line.PHP_EOL));
					}
				}
			}

			fclose($handle);
		}
	}

	function readPage($page)
	{
		$tab = TRUE;
		if (strstr($page, 'css'))
		{
			$tab = FALSE;
			$page = '/'.$page;
		}
		else
		{
			$page = '/includes/'.$page;
		}

		$handle = fopen($_SERVER['DOCUMENT_ROOT'].$page, 'r') or die('Unable to edit '.$page);

		if ($handle)
		{
			while (!feof($handle))
			{
				$line = fgets($handle);

				if (!empty($line))
				{
					if ($tab)
					{
						echo substr($line, 1);
					}
					else
					{
						echo $line;
					}
				}
			}

			fclose($handle);
		}
	}

	function getTitle($page)
	{
		if (!strstr($page, 'body'))
		{
			return;
		}

		$handle = fopen($_SERVER['DOCUMENT_ROOT'].'/'.substr($page, 5), 'r') or die('Unable to read title');

		if ($handle)
		{
			while (!feof($handle))
			{
				$line = fgets($handle);

				if (preg_match('/<title>(.*)<\/title>/', $line, $title))
				{
					fclose($handle);
					return $title[1];
				}
			}

			fclose($handle);
		}
	}

	function setTitle($page, $title)
	{
		$rHandle = fopen($_SERVER['DOCUMENT_ROOT'].'/'.substr($page, 5), 'r') or die('Unable to write title');
		$wHandle = fopen($_SERVER['DOCUMENT_ROOT'].'/'.substr($page, 5).'.tmp', 'w') or die('Unable to write title');

		if ($rHandle && $wHandle)
		{
			while (!feof($rHandle))
			{
				$line = fgets($rHandle);

				if (preg_match('/<title>(.*)<\/title>/', $line))
				{
					fwrite($wHandle, "\t<title>".$title.'</title>'.PHP_EOL);
				}
				else
				{
					fwrite($wHandle, $line);
				}
			}

			fclose($handle);
			rename($_SERVER['DOCUMENT_ROOT'].'/'.substr($page, 5).'.tmp', $_SERVER['DOCUMENT_ROOT'].'/'.substr($page, 5));
		}
	}
?>
