<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/lib/page.php');

	if (isset($_POST['page']) && isset($_POST['body']))
	{
		if (isset($_POST['title']))
		{
			setTitle($_POST['page'], $_POST['title']);
		}

		writePage($_POST['page'], $_POST['body']);

		header('Location: /admin');
	}
?>
<html>
<head>
	<title>Edit Page</title>
	<link rel='icon' href='/images/favicon.ico'>
	<link rel='stylesheet' type='text/css' href='/stdtheme.css'>
</head>

<body>
<?php if (isset($_GET['page'])) { ?>
	<form action='edit.php' method='post' id='edit'>
		<input type='hidden' name='page' value='<?php echo $_GET['page']; ?>'>
		<input type='submit' value='Edit'>
		<?php echo str_replace('body/', '', $_GET['page'])."\n"; $title = getTitle($_GET['page']); if (!empty($title)) { ?>
		<input type='text' name='title' value='<?php echo $title; ?>' placeholder='Title' style='width: 50%; float: right;'></br>
		<?php } else { echo "</br>\n"; } ?>

<!-- Begin text area -->
<textarea name='body' style='box-sizing: border-box; width: 100%; height: 96%; resize: none;'>
<?php readPage($_GET['page']); ?>
</textarea>
<!-- End text area -->

	</form>
<?php } ?>
</body>
</html>
