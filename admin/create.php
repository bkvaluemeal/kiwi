<?php
	if (isset($_POST['page']))
	{
		if (!file_exists($_SERVER['DOCUMENT_ROOT'].'/'.dirname($_POST['page'])))
		{
			mkdir($_SERVER['DOCUMENT_ROOT'].'/'.dirname($_POST['page']), 0755, true);
			mkdir($_SERVER['DOCUMENT_ROOT'].'/includes/body/'.dirname($_POST['page']), 0755, true);
		}

		copy($_SERVER['DOCUMENT_ROOT'].'/res/template/default.php', $_SERVER['DOCUMENT_ROOT'].'/'.$_POST['page']) or die('Could not create '.$_POST['page']);
		touch($_SERVER['DOCUMENT_ROOT'].'/includes/body/'.$_POST['page']) or die('Could not create includes/body/'.$_POST['page']);

		header('Location: /admin/edit.php?page=body/'.$_POST['page']);
	}
?>
