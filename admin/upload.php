<?php
	if (isset($_FILES['file']))
	{
		if (getimagesize($_FILES['file']['tmp_name']))
		{
			$target_file = $_SERVER['DOCUMENT_ROOT'].'/images/'.basename($_FILES['file']['name']);
			move_uploaded_file($_FILES['file']['tmp_name'], $target_file) or die('Could not upload file');
		}

		header('Location: /admin');
	}
?>
