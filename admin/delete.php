<?php
	if (isset($_POST['page']))
	{
		unlink($_SERVER['DOCUMENT_ROOT'].'/'.$_POST['page']) or die('Could not delete '.$_POST['page']);
		unlink($_SERVER['DOCUMENT_ROOT'].'/includes/body/'.$_POST['page']) or die('Could not delete includes/body/'.$_POST['page']);

		header('Location: /admin');
	}

	if (isset($_POST['img']))
	{
		unlink($_SERVER['DOCUMENT_ROOT'].'/images/'.$_POST['img']) or die('Could not delete '.$_POST['img']);

		header('Location: /admin');
	}
?>
