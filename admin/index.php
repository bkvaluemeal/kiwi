<html>
<head>
	<title>Admin Area</title>
	<link rel='icon' href='/images/favicon.ico'>
	<link rel='stylesheet' type='text/css' href='/stdtheme.css'>
</head>

<body>
<?php require_once($_SERVER['DOCUMENT_ROOT'].'/includes/header.php'); ?>
	<table border='1' align='center' style='text-align: center; width: 600px'>
		<tr>
			<td>
				<h1>Admin Area</h1>
				<form action='edit.php' method='get'>
<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/lib/page.php');

	$pages = getPages();

	echo str_repeat("\t", 5)."<select name='page' size='".(count($pages) + 3)."'>".PHP_EOL;

	foreach ($pages as $page)
	{
		echo str_repeat("\t", 6)."<option value='".'body/'.$page."'>".$page."</option>".PHP_EOL;
	}
?>
						<option value='header.php'>header.php</option>
						<option value='footer.php'>footer.php</option>
						<option value='stdtheme.css'>stdtheme.css</option>
					</select>
					<input type='submit' value='Edit Page'>
				</form>
				<form action='create.php' method='post'>
					<input type='text' name='page' style='width: 50%;'>
					<input type='submit' value='Create Page'>
				</form>
				<form action='delete.php' method='post'>
					<input type='text' name='page' style='width: 50%;'>
					<input type='submit' value='Delete Page'>
				</form>
				<form action='upload.php' method='post' enctype='multipart/form-data'>
					<input type='file' name='file' style='width: 50%'>
					<input type='submit' value='Upload Image'>
				</form>
				<form action='delete.php' method='post'>
<?php
	$imgs = scandir($_SERVER['DOCUMENT_ROOT'].'/images');

	echo str_repeat("\t", 5)."<select name='img' size='".(count($imgs) - 2)."'>".PHP_EOL;

	foreach ($imgs as $img)
	{
		if ($img !== '.' && $img !== '..')
		{
			echo str_repeat("\t", 6)."<option value='".$img."'>".$img."</option>".PHP_EOL;
		}
	}
?>
					</select>
					<input type='submit' value='Delete Image'>
				</form>
			</td>
		</tr>
	</table>
<?php require_once($_SERVER['DOCUMENT_ROOT'].'/includes/footer.php'); ?>
</body>
</html>
